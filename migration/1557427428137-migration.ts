import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1557427428137 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `flat_type` (`id` int NOT NULL, `name` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `building` (`id` varchar(255) NOT NULL, `num` varchar(255) NULL, `buildingAddr` varchar(255) NULL, `buildingPhase` int NULL, `buildingAddrRegion` varchar(255) NULL, `buildingAddrDistr` varchar(255) NULL, `corpusDelivery` datetime NULL, `corpusPostAddr` varchar(255) NULL, `corpusPhase` int NULL, `buildingObjectComplexId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `section` (`id` varchar(255) NOT NULL, `num` varchar(255) NULL, `floorCount` int NULL, `garbage` varchar(255) NULL, `lift` varchar(255) NULL, `liftPassenger` varchar(255) NULL, `liftCargo` varchar(255) NULL, `buildingId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `floor` (`id` varchar(255) NOT NULL, `num` varchar(255) NULL, `sectionId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `feature` (`id` int NOT NULL, `name` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `flat` (`id` int NOT NULL, `num` varchar(255) NULL, `id1C` int NULL, `floorNum` int NULL, `numOnFloor` int NULL, `rooms` int NULL, `priceMetr` int NULL, `priceMetrSale` int NULL, `priceTot` int NULL, `priceTotSale` int NULL, `salePercent` int NULL, `furniture` tinyint NOT NULL DEFAULT 0, `decoration` varchar(255) NULL, `squareLive` decimal NULL, `squareTot` decimal NULL, `squareKitchen` decimal NULL, `squareBath` decimal NULL, `squareToilet` decimal NULL, `balconySquare` decimal NULL, `parking` varchar(255) NULL, `type` int NULL, `flatUrl` varchar(255) NULL, `balconyQuantity` int NULL, `floorId` varchar(255) NULL, `flatTypeId` int NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `picture` (`url` varchar(255) NOT NULL, `fileName` varchar(255) NULL, `dow` tinyint NULL DEFAULT 0, `flatId` int NULL, `developerName` varchar(255) NULL, `buildingObjectComplexId` int NULL, UNIQUE INDEX `REL_bc00156d6e7f5afc5c4301250d` (`flatId`), UNIQUE INDEX `REL_e00a80f9b04fc75cfba517dba6` (`developerName`), PRIMARY KEY (`url`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `developer` (`name` varchar(255) NOT NULL, `phone` varchar(255) NULL, `urlSite` varchar(255) NULL, PRIMARY KEY (`name`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `sales_info` (`phone` varchar(255) NOT NULL, `address` varchar(255) NULL, `openAt` varchar(255) NULL, `closeAt` varchar(255) NULL, PRIMARY KEY (`phone`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `region` (`regionName` varchar(255) NOT NULL, PRIMARY KEY (`regionName`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `building_object` (`complexId` int NOT NULL, `complecxName` varchar(255) NULL, `description` longtext NULL, `district` varchar(255) NULL, `address` varchar(255) NULL, `longitude` decimal NULL, `latitude` decimal NULL, `developerName` varchar(255) NULL, `salesInfoPhone` varchar(255) NULL, `regionRegionName` varchar(255) NULL, PRIMARY KEY (`complexId`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `mortgage_program` (`id1C` varchar(255) NOT NULL, `name` varchar(255) NULL, `minPervVznos` decimal NULL, `procStav` decimal NULL, `maxSrok` int NULL, `maxAmount` int NULL, `type` int NULL, `bankBankId1C` int NULL, PRIMARY KEY (`id1C`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `bank` (`bankId1C` int NOT NULL, `bankName` varchar(255) NULL, `licenseNum` int NULL, `licenseDate` datetime NULL, PRIMARY KEY (`bankId1C`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `building` ADD CONSTRAINT `FK_ac16aad80e25e5271dbfc19f9f4` FOREIGN KEY (`buildingObjectComplexId`) REFERENCES `building_object`(`complexId`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `section` ADD CONSTRAINT `FK_9002fbc5acb8d863d9a340d225a` FOREIGN KEY (`buildingId`) REFERENCES `building`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `floor` ADD CONSTRAINT `FK_c68bd261ae11c0aa4258231daad` FOREIGN KEY (`sectionId`) REFERENCES `section`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `flat` ADD CONSTRAINT `FK_9b02ffd680bd62d821b35965b47` FOREIGN KEY (`floorId`) REFERENCES `floor`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `flat` ADD CONSTRAINT `FK_16955b4058ca4122f98bdcef9f6` FOREIGN KEY (`flatTypeId`) REFERENCES `flat_type`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `picture` ADD CONSTRAINT `FK_bc00156d6e7f5afc5c4301250d9` FOREIGN KEY (`flatId`) REFERENCES `flat`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `picture` ADD CONSTRAINT `FK_e00a80f9b04fc75cfba517dba69` FOREIGN KEY (`developerName`) REFERENCES `developer`(`name`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `picture` ADD CONSTRAINT `FK_673d0c5b123306447855bd82c5f` FOREIGN KEY (`buildingObjectComplexId`) REFERENCES `building_object`(`complexId`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `building_object` ADD CONSTRAINT `FK_14afe5ce6f4542aeb3f7e9f388b` FOREIGN KEY (`developerName`) REFERENCES `developer`(`name`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `building_object` ADD CONSTRAINT `FK_46a234e027d801f1a19692d5ec6` FOREIGN KEY (`salesInfoPhone`) REFERENCES `sales_info`(`phone`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `building_object` ADD CONSTRAINT `FK_06f875f147541bf340f52c2925a` FOREIGN KEY (`regionRegionName`) REFERENCES `region`(`regionName`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `mortgage_program` ADD CONSTRAINT `FK_2fb3f7665aeb3ff3be2ba96a88b` FOREIGN KEY (`bankBankId1C`) REFERENCES `bank`(`bankId1C`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `mortgage_program` DROP FOREIGN KEY `FK_2fb3f7665aeb3ff3be2ba96a88b`");
        await queryRunner.query("ALTER TABLE `building_object` DROP FOREIGN KEY `FK_06f875f147541bf340f52c2925a`");
        await queryRunner.query("ALTER TABLE `building_object` DROP FOREIGN KEY `FK_46a234e027d801f1a19692d5ec6`");
        await queryRunner.query("ALTER TABLE `building_object` DROP FOREIGN KEY `FK_14afe5ce6f4542aeb3f7e9f388b`");
        await queryRunner.query("ALTER TABLE `picture` DROP FOREIGN KEY `FK_673d0c5b123306447855bd82c5f`");
        await queryRunner.query("ALTER TABLE `picture` DROP FOREIGN KEY `FK_e00a80f9b04fc75cfba517dba69`");
        await queryRunner.query("ALTER TABLE `picture` DROP FOREIGN KEY `FK_bc00156d6e7f5afc5c4301250d9`");
        await queryRunner.query("ALTER TABLE `flat` DROP FOREIGN KEY `FK_16955b4058ca4122f98bdcef9f6`");
        await queryRunner.query("ALTER TABLE `flat` DROP FOREIGN KEY `FK_9b02ffd680bd62d821b35965b47`");
        await queryRunner.query("ALTER TABLE `floor` DROP FOREIGN KEY `FK_c68bd261ae11c0aa4258231daad`");
        await queryRunner.query("ALTER TABLE `section` DROP FOREIGN KEY `FK_9002fbc5acb8d863d9a340d225a`");
        await queryRunner.query("ALTER TABLE `building` DROP FOREIGN KEY `FK_ac16aad80e25e5271dbfc19f9f4`");
        await queryRunner.query("DROP TABLE `bank`");
        await queryRunner.query("DROP TABLE `mortgage_program`");
        await queryRunner.query("DROP TABLE `building_object`");
        await queryRunner.query("DROP TABLE `region`");
        await queryRunner.query("DROP TABLE `sales_info`");
        await queryRunner.query("DROP TABLE `developer`");
        await queryRunner.query("DROP INDEX `REL_e00a80f9b04fc75cfba517dba6` ON `picture`");
        await queryRunner.query("DROP INDEX `REL_bc00156d6e7f5afc5c4301250d` ON `picture`");
        await queryRunner.query("DROP TABLE `picture`");
        await queryRunner.query("DROP TABLE `flat`");
        await queryRunner.query("DROP TABLE `feature`");
        await queryRunner.query("DROP TABLE `floor`");
        await queryRunner.query("DROP TABLE `section`");
        await queryRunner.query("DROP TABLE `building`");
        await queryRunner.query("DROP TABLE `flat_type`");
    }

}
