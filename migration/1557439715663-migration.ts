import {MigrationInterface, QueryRunner} from "typeorm";

export class migration1557439715663 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `picture` DROP COLUMN `dow`");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `picture` ADD `dow` tinyint NULL DEFAULT '0'");
    }

}
