import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn, JoinColumn, PrimaryColumn } from 'typeorm';
import { Flat } from '../flat/flat.entity';
import { BuildingObject } from '../building-object/building-object.entity';
import { Developer } from '../developer/developer.entity';

@Entity()
export class Picture {
  @PrimaryColumn()
  url: string;

  @Column({ nullable: true })
  fileName: string;

  @OneToOne(type => Flat, flat => flat.plan)
  @JoinColumn()
  flat: Flat;

  @OneToOne(type => Developer, developer => developer.logo)
  @JoinColumn()
  developer: Developer;

  @ManyToOne(type => BuildingObject, buildingObject => buildingObject.pictures)
  buildingObject: BuildingObject;

  async parsAttributes(url): Promise<Picture> {

    this.url = url;

    return this;
  }
}
