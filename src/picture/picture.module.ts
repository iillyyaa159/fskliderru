import { Module } from '@nestjs/common';
import { PictureService } from './picture.service';
import { Picture } from './picture.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Picture])],
  providers: [PictureService],
  exports: [PictureService],
})
export class PictureModule {}
