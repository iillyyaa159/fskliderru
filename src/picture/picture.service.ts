import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Picture } from './picture.entity';
import { Repository } from 'typeorm';
import { Sleep } from 'src/helpers/sleep';
import * as fs from 'fs';
import * as request from 'request-promise-native';
import { promisify } from 'util';

const writeFile = promisify(fs.writeFile);

const SLEEP = 5 * 60 * 1000;

@Injectable()
export class PictureService {
  constructor(
    @InjectRepository(Picture)
    private readonly repositoryPicture: Repository<Picture>,
  ) {
    this.savePicture();
  }

  async getNoDownloads(): Promise<Picture> {
    return await this.repositoryPicture.findOne({
      relations: ['flat', 'developer', 'buildingObject'],
      where: { fileName: null },
    });
  }

  async savePicture() {
    while (true) {
      try {
        const picture = await this.getNoDownloads();

        if (!picture) {
          console.log('Картинки закончились');
          await Sleep(SLEEP);
          continue;
        }

        console.time('Время сохранения картинки');

        let path = `./pictures/`;
        let idObject;

        if (!fs.existsSync(path)) {
          fs.mkdirSync(path);
        }

        switch (true) {
          case picture.buildingObject !== null:
            path += 'buildingObject/';
            idObject = picture.buildingObject.complexId;
            break;
          case picture.developer !== null:
            path += 'developer/';
            idObject = picture.developer.name;
            break;
          case picture.flat !== null:
            path += 'flat/';
            idObject = picture.flat.id;
            break;
          default:
            console.log(`Косяк, ${picture}`);
            continue;
        }

        if (!fs.existsSync(path)) {
          fs.mkdirSync(path);
        }

        path += idObject;

        if (!fs.existsSync(path)) {
          fs.mkdirSync(path);
        }

        const arrayUrl = picture.url.split('/');
        picture.fileName = arrayUrl[arrayUrl.length - 1];

        const body = await request(picture.url, { encoding: 'binary' });

        await writeFile(path + '/' + picture.fileName, body, 'binary');

        // await new Promise((resolve, reject) => {
        //   request(picture.url, { encoding: 'binary' }, (error, response, body) => {
        //     if (error) {
        //       reject(error);
        //     }
        //     fs.writeFile(path + '/' + picture.fileName, body, 'binary', (err) => {
        //       if (err) {
        //         console.error(err);
        //       }
        //     });
        //     resolve();
        //   });
        // });

        await this.repositoryPicture.save(picture);

        console.timeEnd('Время сохранения картинки');
        console.log('Кратинка сохранилась');
      } catch (err) {
        console.error(err);
      }
    }
  }
}
