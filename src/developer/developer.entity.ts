import { Column, Entity, OneToOne, PrimaryGeneratedColumn, PrimaryColumn, OneToMany } from 'typeorm';
import { Picture } from '../picture/picture.entity';
import { BuildingObject } from '../building-object/building-object.entity';

@Entity()
export class Developer {
  @PrimaryColumn()
  name: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ nullable: true })
  urlSite: string;

  @OneToOne(type => Picture, picture => picture.developer, {
    cascade: true,
  })
  logo: Picture;

  @OneToMany(type => BuildingObject, buildingObject => buildingObject.developer)
  buildingObject: BuildingObject;

  async parsAttributes(developer): Promise<Developer> {

    this.name = developer.Name._text;

    if (developer.Phone) {
      this.phone = developer.Phone._text;
    }

    if (developer.Site) {
      this.urlSite = developer.Site._text;
    }

    if (developer.Logo) {
      const url = 'https://fsk-lider.ru' + developer.Logo._text;
      this.logo = await new Picture().parsAttributes(url);
    }

    return this;
  }
}
