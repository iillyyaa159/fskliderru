import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { Floor } from '../floor/floor.entity';
import { Building } from '../building/building.entity';
import { Flat } from '../flat/flat.entity';

@Entity()
export class Section {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: true })
  num: string;

  @Column({ nullable: true })
  floorCount: number;

  @Column({ nullable: true })
  garbage: string;

  @Column({ nullable: true })
  lift: string;

  @Column({ nullable: true })
  liftPassenger: string;

  @Column({ nullable: true })
  liftCargo: string;

  @OneToMany(type => Floor, floor => floor.section, {
    cascade: true,
  })
  floors: Floor[];

  @ManyToOne(type => Building, building => building.sections)
  building: Building;

  async parsAttributes(idBuilding, section): Promise<Section> {
    const atributs = section._attributes;
    const floors = section.Floor;

    this.id = idBuilding + '_' + atributs.Num;
    this.num = atributs.Num;
    this.floorCount = atributs.Floor_Count ? atributs.Floor_Count * 1 : undefined;
    this.garbage = atributs.Garbage;
    this.lift = atributs.Lift;
    this.liftCargo = atributs.Lift_Cargo;
    this.liftPassenger = atributs.Lift_Passenger;

    if (floors._attributes) {
      this.floors = Array<Floor>();
      this.floors.push(await new Floor().parsAttributes(this.id, floors));
    } else {
      this.floors = Array<Floor>();
      for (const floor of floors) {
        this.floors.push(await new Floor().parsAttributes(this.id, floor));
      }
    }

    return this;
  }
}
