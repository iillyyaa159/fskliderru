import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn } from 'typeorm';
import { Section } from '../section/section.entity';
import { BuildingObject } from '../building-object/building-object.entity';

@Entity()
export class Building {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: true })
  num: string;

  @Column({ nullable: true })
  buildingAddr: string;

  @Column({ nullable: true })
  buildingPhase: number;

  @Column({ nullable: true })
  buildingAddrRegion: string;

  @Column({ nullable: true })
  buildingAddrDistr: string;

  @Column({ nullable: true })
  corpusDelivery: Date;

  @Column({ nullable: true })
  corpusPostAddr: string;

  @Column({ nullable: true })
  corpusPhase: number;

  @OneToMany(() => Section, section => section.building, {
    cascade: true,
  })
  sections: Section[];

  @ManyToOne(() => BuildingObject, buildingObject => buildingObject.buildings)
  buildingObject: BuildingObject;

  async parsAttributes(idBuildingObject, building): Promise<Building> {
    const atributs = building._attributes;
    const sections = building.Section;

    this.id = idBuildingObject.toString() + '_' + atributs.Num;
    this.num = atributs.Num;
    this.buildingAddr = atributs.Building_Addr;
    this.buildingPhase = atributs.Building_Phase ? atributs.Building_Phase * 1 : undefined;
    this.buildingAddrRegion = atributs.Building_Addr_Region;
    this.buildingAddrDistr = atributs.Building_Addr_Distr;
    this.corpusDelivery = atributs.Corpus_Delivery;
    this.corpusPostAddr = atributs.Corpus_Post_Addr;
    this.corpusPhase = atributs.Corpus_Phase ? atributs.Corpus_Phase * 1 : undefined;

    if (sections._attributes) {
      this.sections = Array<Section>();
      this.sections.push(await new Section().parsAttributes(this.id, sections));
    } else {
      this.sections = Array<Section>();
      for (const section of sections) {
        this.sections.push(await new Section().parsAttributes(this.id, section));
      }
    }

    return this;
  }
}
