import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { MortgageProgram } from '../mortgage-program/mortgage-program.entity';
import { Developer } from '../developer/developer.entity';
import { SalesInfo } from '../sales-info/sales-info.entity';
import { Building } from '../building/building.entity';
import { Picture } from '../picture/picture.entity';
import { Region } from '../region/region.entity';
import { Flat } from '../flat/flat.entity';

@Entity()
export class BuildingObject {
  @PrimaryColumn()
  complexId: number;

  @Column({ nullable: true })
  complecxName: string;

  @Column({ type: 'longtext', nullable: true })
  description: string;

  @Column({ nullable: true })
  district: string;

  @Column({ nullable: true })
  address: string;

  @Column({ type: 'decimal', nullable: true })
  longitude: number;

  @Column({ type: 'decimal', nullable: true })
  latitude: number;

  @ManyToOne(type => Developer, developer => developer.buildingObject , {
    cascade: true,
  })
  developer: Developer;

  @ManyToOne(type => SalesInfo, salesInfo => salesInfo.buildingObject, {
    cascade: true,
  })
  salesInfo: SalesInfo;

  @OneToMany(type => Picture, picture => picture.buildingObject, {
    cascade: true,
  })
  pictures: Picture[];

  @OneToMany(type => Building, building => building.buildingObject, {
    cascade: true,
  })
  buildings: Building[];

  @ManyToOne(type => Region, region => region.buildingObjects)
  region: Region;

  @ManyToMany(type => MortgageProgram, mortgageProgram => mortgageProgram.buildingObjects, {
    cascade: true,
  })
  mortgagePrograms: MortgageProgram[];

  async parsAttributes(buildingObjectObj): Promise<BuildingObject> {
    const atributs = buildingObjectObj._attributes;
    const info = buildingObjectObj.Info;
    const images = buildingObjectObj.Info.Images.Image;
    const mortgages = buildingObjectObj._attributes.Mortgages;
    const buildings = buildingObjectObj.Buildings.Building;

    this.complecxName = atributs.Complex_name;
    this.complexId = atributs.Complex_id ? atributs.Complex_id * 1 : undefined;
    this.description = info.Description._cdata;
    this.district = info.District._text;
    this.address = info.Address._text;
    this.latitude = info.Latitude._text ? info.Latitude._text * 1 : undefined;
    this.longitude = info.Longitude._text ? info.Longitude._text * 1 : undefined;

    this.developer = await new Developer().parsAttributes(buildingObjectObj.developer);
    this.salesInfo = await new SalesInfo().parsAttributes(buildingObjectObj.Sales_Info);

    if (images) {
      this.pictures = Array<Picture>();
      for (const image of images) {
        const url = 'https://fsk-lider.ru' + image._text;
        this.pictures.push(await new Picture().parsAttributes(url));
      }
    }

    // this.mortgagePrograms = [];
    // for (const image of images) {
    //   this.mortgagePrograms.push(await new MortgageProgram().parsAttributes(image));
    // }

    if (buildings._attributes) {
      this.buildings = Array<Building>();
      this.buildings.push(await new Building().parsAttributes(this.complexId, buildings));
    } else {
      this.buildings = Array<Building>();
      for (const building of buildings) {
        this.buildings.push(await new Building().parsAttributes(this.complexId, building));
      }
    }

    return this;
  }
}
