import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToOne, PrimaryColumn } from 'typeorm';
import { FlatType } from '../flat-type/flat-type.entity';
import { Picture } from '../picture/picture.entity';
import { Floor } from '../floor/floor.entity';
import { Feature } from '../feature/feature.entity';

@Entity()
export class Flat {
  @PrimaryColumn()
  id: number;

  @Column({ nullable: true })
  num: string;

  @Column({ nullable: true })
  id1C: number;

  @Column({ nullable: true })
  floorNum: number;

  @Column({ nullable: true })
  numOnFloor: number;

  @Column({ nullable: true })
  rooms: number;

  @Column({ nullable: true })
  priceMetr: number;

  @Column({ nullable: true })
  priceMetrSale: number;

  @Column({ nullable: true })
  priceTot: number;

  @Column({ nullable: true })
  priceTotSale: number;

  @Column({ nullable: true })
  salePercent: number;

  @Column({ default: false })
  furniture: boolean;

  @Column({ nullable: true })
  decoration: string;

  @Column({ type: 'decimal', nullable: true })
  squareLive: number;

  @Column({ type: 'decimal', nullable: true })
  squareTot: number;

  @Column({ type: 'decimal', nullable: true })
  squareKitchen: number;

  @Column({ type: 'decimal', nullable: true })
  squareBath: number;

  @Column({ type: 'decimal', nullable: true })
  squareToilet: number;

  @Column({ type: 'decimal', nullable: true })
  balconySquare: number;

  @Column({ nullable: true })
  parking: string;

  @Column({ nullable: true })
  type: number;

  @Column({ nullable: true })
  flatUrl: string;

  @Column({ nullable: true })
  balconyQuantity: number;

  @OneToOne(type => Picture, picture => picture.flat, {
    cascade: true,
  })
  plan: Picture;

  @ManyToOne(type => Floor, floor => floor.flats)
  floor: Floor;

  @ManyToOne(type => FlatType, flatType => flatType.flats)
  flatType: FlatType;

  @ManyToMany(type => Feature, feature => feature.flats)
  features: Feature[];

  async parsAttributes(flat): Promise<Flat> {
    const atributs = flat._attributes;

    this.id = atributs.Id  * 1;
    this.num = atributs.Number;
    this.floorNum = atributs.Floor ? atributs.Floor * 1 : undefined;
    this.numOnFloor = atributs.Num_on_floor ? atributs.Num_on_floor * 1 : undefined;
    this.rooms = atributs.Rooms ? atributs.Rooms * 1 : undefined;
    this.priceMetr = atributs.Price_metr ? atributs.Price_metr * 1 : undefined;
    this.priceMetrSale = atributs.Price_metr_sale ? atributs.Price_metr_sale * 1 : undefined;
    this.priceTot = atributs.Price_tot ? atributs.Price_tot * 1 : undefined;
    this.priceTotSale = atributs.Price_tot_sale ? atributs.Price_tot_sale * 1 : undefined;
    this.salePercent = atributs.Sale_percent ? atributs.Sale_percent * 1 : undefined;
    this.furniture = Boolean(atributs.Furniture);
    this.decoration = atributs.Decoration;
    this.squareLive = atributs.Square_live ? atributs.Square_live * 1 : undefined;
    this.squareTot = atributs.Square_tot ? atributs.Square_tot * 1 : undefined;
    this.squareKitchen = atributs.Square_kitchen ? atributs.Square_kitchen * 1 : undefined;
    this.squareBath = atributs.Square_bath ? atributs.Square_bath * 1 : undefined;
    this.squareToilet = atributs.Square_toilet ? atributs.Square_toilet * 1 : undefined;
    this.balconySquare = atributs.Balcony_square_1 ? atributs.Balcony_square_1 * 1 : undefined;
    this.parking = atributs.Parking;
    this.type = atributs.Type ? atributs.Type * 1 : undefined;
    this.flatUrl = atributs.FlatUrl;
    this.balconyQuantity = atributs.Balcony_quantity ? atributs.Balcony_quantity * 1 : undefined;

    if (atributs.Plan) {
      this.plan = await new Picture().parsAttributes(atributs.Plan);
    }

    return this;
  }
}
