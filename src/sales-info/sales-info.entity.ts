import { Column, Entity, PrimaryColumn, OneToMany } from 'typeorm';
import { BuildingObject } from '../building-object/building-object.entity';

@Entity()
export class SalesInfo {
  @PrimaryColumn()
  phone: string;

  @Column({ nullable: true })
  address: string;

  @Column({ nullable: true })
  openAt: string;

  @Column({ nullable: true })
  closeAt: string;

  @OneToMany(() => BuildingObject, buildingObject => buildingObject.salesInfo)
  buildingObject: BuildingObject;

  async parsAttributes(salesInfo): Promise<SalesInfo> {

    this.phone = salesInfo.Phone._text;
    this.address = salesInfo.Address._text;
    this.openAt = salesInfo.Open_at._text;
    this.closeAt = salesInfo.Close_at._text;

    return this;
  }
}
