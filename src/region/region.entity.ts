import { Column, Entity, OneToMany, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';
import { BuildingObject } from '../building-object/building-object.entity';
import { MortgageProgram } from '../mortgage-program/mortgage-program.entity';

@Entity()
export class Region {
  @PrimaryColumn()
  regionName: string;

  @OneToMany(type => BuildingObject, buildingObject => buildingObject.region, {
    cascade: true,
  })
  buildingObjects: BuildingObject[];

  async parsAttributes(regionObj): Promise<Region> {
    const atributs = regionObj._attributes;
    const objects = regionObj.Object;

    this.regionName = atributs.Region_Name;
    this.buildingObjects = [];

    if (objects) {
      for (const buildingObject of objects) {
        this.buildingObjects.push(await new BuildingObject().parsAttributes(buildingObject));
      }
    }

    return this;
  }
}
