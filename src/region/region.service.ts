import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Region } from './region.entity';

@Injectable()
export class RegionService {
  constructor(
    @InjectRepository(Region)
    private readonly regionRepository: Repository<Region>,
  ) {}

  async saveMulti(regions) {
    const regionsObj = Array<Region>();

    // for (const region of regions) {
    //   regionsObj.push(await new Region().parsAttributes(region));
    // }

    regionsObj.push(await new Region().parsAttributes(regions[0]));

    return await this.regionRepository.save(regionsObj);
  }
}
