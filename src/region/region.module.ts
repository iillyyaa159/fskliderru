import { Module } from '@nestjs/common';
import { RegionService } from './region.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Region } from './region.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Region])],
  providers: [RegionService],
  exports: [RegionService],
})
export class RegionModule {}
