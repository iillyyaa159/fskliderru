import { Column, Entity, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Flat } from '../flat/flat.entity';
import { Section } from '../section/section.entity';

@Entity()
export class Floor {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: true })
  num: string;

  @OneToMany(type => Flat, flat => flat.floor, {
    cascade: true,
  })
  flats: Flat[];

  @ManyToOne(type => Section, section => section.floors)
  section: Section;

  async parsAttributes(idSection, floor) {
    const atributs = floor._attributes;
    const flats = floor.Flat;

    this.id = idSection + '_' + atributs.Num;
    this.num = atributs.Num;

    if (!flats) {
      return this;
    }

    if (flats._attributes) {
      this.flats = Array<Flat>();
      this.flats.push(await new Flat().parsAttributes(flats));
    } else {
      this.flats = Array<Flat>();
      for (const flat of flats) {
        this.flats.push(await new Flat().parsAttributes(flat));
      }
    }

    return this;
  }
}
