import { Column, Entity, JoinColumn, ManyToMany, PrimaryColumn } from 'typeorm';
import { Flat } from '../flat/flat.entity';

@Entity()
export class Feature {
  @PrimaryColumn()
  id: number;

  @Column({ nullable: true })
  name: string;

  @ManyToMany(type => Flat, flat => flat.features)
  @JoinColumn()
  flats: Flat[];

  async parsAttributes(featureObj): Promise<Feature> {
    const atributs = featureObj._attributes;

    this.id = atributs.FeatureID ? atributs.FeatureID * 1 : undefined;
    this.name = atributs.FeatureName;

    return this;
  }
}
