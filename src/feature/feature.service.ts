import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Feature } from './feature.entity';

@Injectable()
export class FeatureService {
  constructor(
    @InjectRepository(Feature)
    private readonly featureRepository: Repository<Feature>,
  ) {}

  async saveMulti(features) {
    const featuresObj = Array<Feature>();

    for (const feature of features) {
      featuresObj.push(await new Feature().parsAttributes(feature));
    }

    return await this.featureRepository.save(featuresObj);
  }
}
