import { HttpService, Injectable } from '@nestjs/common';
import * as convert from 'xml-js';
import * as request from 'request';
import * as fs from 'fs';
import { BankService } from '../bank/bank.service';
import { FlatTypeService } from '../flat-type/flat-type.service';
import { FeatureService } from '../feature/feature.service';
import { RegionService } from '../region/region.service';
import { Sleep } from 'src/helpers/sleep';

const SLEEP_TIME = 5 * 60 * 1000;
const URL = 'https://fsk-lider.ru/export/universal.xml';

@Injectable()
export class ParserService {
  constructor(
    private readonly bankService: BankService,
    private readonly flatTypeService: FlatTypeService,
    private readonly featureService: FeatureService,
    private readonly regionService: RegionService,
  ) {
    this.start();
  }

  async start() {
    while (true) {
      try {
        console.time('Get doc');
        const doc = await this.getDoc();
        console.timeEnd('Get doc');

        console.time('Save doc');
        await this.saveDoc(doc);
        console.timeEnd('Save doc');

        console.log(`Sleep ${SLEEP_TIME}`);
        console.log(new Date());
        await Sleep(SLEEP_TIME);
      } catch (error) {
        console.error(error);
      }
    }
  }

  async getDoc() {
    try {
      // TODO переписать
      let data;

      do {
        data = await new Promise((resolve, reject) => {
          request(URL, (error, response, body) => {
            if (body) {
              resolve(body);
            }
          });
        });
      } while (!data);

      // @ts-ignore
      return convert.xml2js(data.toString(), { compact: true }).Data;
    } catch (error) {
      console.error(error);
    }
  }

  private async saveDoc(doc: any) {
    console.time('Save banks');
    const banks = await this.bankService.saveMulti(doc.Banks.Bank);
    console.timeEnd('Save banks');

    console.time('Save flat type');
    const flatTypes = await this.flatTypeService.saveMulti(doc.FlatTypes.FlatType);
    console.timeEnd('Save flat type');

    console.time('Save feature');
    const features = await this.featureService.saveMulti(doc.Features.Feature);
    console.timeEnd('Save feature');

    console.time('Save region and all');
    const regions = await this.regionService.saveMulti(doc.Regions.Region);
    console.timeEnd('Save region and all');
  }
}
