import { HttpModule, Module } from '@nestjs/common';
import { ParserService } from './parser.service';
import { BankModule } from '../bank/bank.module';
import { FlatTypeModule } from '../flat-type/flat-type.module';
import { FeatureModule } from '../feature/feature.module';
import { RegionModule } from '../region/region.module';
import { PictureModule } from '../picture/picture.module';

@Module({
  imports: [HttpModule, BankModule, FlatTypeModule, FeatureModule, RegionModule, PictureModule],
  providers: [ParserService],
})
export class ParserModule {}
