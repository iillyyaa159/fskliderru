import { Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { Bank } from '../bank/bank.entity';
import { BuildingObject } from '../building-object/building-object.entity';

@Entity()
export class MortgageProgram {
  // @PrimaryGeneratedColumn()
  // id: number;

  @PrimaryColumn()
  id1C: string;

  @Column({ nullable: true })
  name: string;

  @Column({ type: 'decimal', nullable: true })
  minPervVznos: number;

  @Column({ type: 'decimal', nullable: true })
  procStav: number;

  @Column({ nullable: true })
  maxSrok: number;

  @Column({ nullable: true })
  maxAmount: number;

  @Column({ nullable: true })
  type: number;

  @ManyToOne(type => Bank, bank => bank.mortgagePrograms)
  bank: Bank;

  @ManyToMany(type => BuildingObject, buildingObject => buildingObject.mortgagePrograms)
  @JoinColumn()
  buildingObjects: BuildingObject[];

  async parsAttributes(mortPrObject): Promise<MortgageProgram> {
    const atributs = mortPrObject._attributes;

    this.id1C = atributs.ID1C;
    this.maxAmount = atributs.MaxAmount ? atributs.MaxAmount * 1 : undefined;
    this.maxSrok = atributs.MaxSrok ? atributs.MaxSrok * 1 : undefined;
    this.minPervVznos = atributs.MinPervVznos ? atributs.MinPervVznos * 1 : undefined;
    this.name = atributs.Name;
    this.procStav = atributs.ProcStav ? atributs.ProcStav * 1 : undefined;
    this.type = atributs.Type ? atributs.Type * 1 : undefined;

    return this;
  }
}
