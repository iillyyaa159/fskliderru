import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BankModule } from './bank/bank.module';
import { MortgageProgramModule } from './mortgage-program/mortgage-program.module';
import { FlatTypeModule } from './flat-type/flat-type.module';
import { FeatureModule } from './feature/feature.module';
import { RegionModule } from './region/region.module';
import { PictureModule } from './picture/picture.module';
import { SectionModule } from './section/section.module';
import { BuildingModule } from './building/building.module';
import { SalesInfoModule } from './sales-info/sales-info.module';
import { BuildingObjectModule } from './building-object/building-object.module';
import { FloorModule } from './floor/floor.module';
import { FlatModule } from './flat/flat.module';
import { DeveloperModule } from './developer/developer.module';
import { ParserModule } from './parser/parser.module';

@Module({
  imports: [TypeOrmModule.forRoot(), BankModule, MortgageProgramModule, FlatTypeModule,
    FeatureModule, RegionModule, PictureModule, SectionModule, BuildingModule, SalesInfoModule,
    BuildingObjectModule, FloorModule, FlatModule, DeveloperModule, ParserModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
