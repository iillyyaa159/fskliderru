import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { FlatType } from './flat-type.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class FlatTypeService {
  constructor(
    @InjectRepository(FlatType)
    private readonly flatTypeRepository: Repository<FlatType>,
  ) {}

  async saveMulti(flatTypes) {
    const flatTypesObj = Array<FlatType>();

    for (const flatType of flatTypes) {
      flatTypesObj.push(await new FlatType().parsAttributes(flatType));
    }

    return await this.flatTypeRepository.save(flatTypesObj);
  }
}
