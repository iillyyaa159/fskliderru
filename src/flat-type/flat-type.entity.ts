import { Column, Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { Flat } from '../flat/flat.entity';
import { isUndefined } from 'util';

@Entity()
export class FlatType {
  @PrimaryColumn()
  id: number;

  @Column({ nullable: true })
  name: string;

  @OneToMany(type => Flat, flat => flat.flatType)
  flats: Flat[];

  async parsAttributes(flatTypeObj): Promise<FlatType> {
    const atributs = flatTypeObj._attributes;

    this.id = atributs.ID ? atributs.ID * 1 : undefined;
    this.name = atributs.Name;

    return this;
  }
}
