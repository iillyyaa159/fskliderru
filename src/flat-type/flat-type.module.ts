import { Module } from '@nestjs/common';
import { FlatTypeService } from './flat-type.service';
import { Bank } from '../bank/bank.entity';
import { FlatType } from './flat-type.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([FlatType])],
  providers: [FlatTypeService],
  exports: [FlatTypeService],
})
export class FlatTypeModule {}
