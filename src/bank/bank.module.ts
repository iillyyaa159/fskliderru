import { Module } from '@nestjs/common';
import { BankService } from './bank.service';
import { Bank } from './bank.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Bank])],
  providers: [BankService],
  exports: [BankService],
})
export class BankModule {}
