import { Column, Entity, OneToMany, PrimaryColumn, PrimaryGeneratedColumn } from 'typeorm';
import { MortgageProgram } from '../mortgage-program/mortgage-program.entity';

@Entity()
export class Bank {
  @PrimaryColumn()
  bankId1C: number;

  @Column({ nullable: true })
  bankName: string;

  @Column({ nullable: true })
  licenseNum: number;

  @Column({ nullable: true })
  licenseDate: Date;

  @OneToMany(type => MortgageProgram, mortgageProgram => mortgageProgram.bank, {
    cascade: true,
  })
  mortgagePrograms: MortgageProgram[];

  async parsAttributes(bankObj): Promise<Bank> {
    const atributs = bankObj._attributes;

    this.bankName = atributs.BankName;
    this.bankId1C = atributs.BankID1C ? atributs.BankID1C * 1 : undefined;
    this.licenseDate = atributs.LicenseDate;
    this.licenseNum = atributs.LicenseNum ? atributs.LicenseNum * 1 : undefined;
    this.mortgagePrograms = [];

    if (!bankObj.MortgageProgram._attributes) {
      for (const mortPr of bankObj.MortgageProgram) {
        this.mortgagePrograms.push(await new MortgageProgram().parsAttributes(mortPr));
      }
    } else {
      this.mortgagePrograms.push(await new MortgageProgram().parsAttributes(bankObj.MortgageProgram));
    }

    return this;
  }
}
