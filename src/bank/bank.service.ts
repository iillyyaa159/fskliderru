import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Bank } from './bank.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BankService {
  constructor(
    @InjectRepository(Bank)
    private readonly bankRepository: Repository<Bank>,
  ) {
  }

  async saveMulti(banks) {
    const banksObj = Array<Bank>();

    for (const bank of banks) {
      banksObj.push(await new Bank().parsAttributes(bank));
    }

    return await this.bankRepository.save(banksObj);
  }
}
